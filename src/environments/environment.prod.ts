export const environment = {
  production: true,
  BASE_URL: 'http://localhost:8000/',
  WS_URL: 'ws://localhost:8000/'
};