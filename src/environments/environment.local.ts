export const environment = {
    production: false,
    BASE_URL: 'http://localhost:8000/',
    WS_URL: 'ws://localhost:8000/'
  };