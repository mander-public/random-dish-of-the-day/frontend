import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CoreRoutingModule } from './core-routing.module';
import { MainCoreComponent } from './views/main-core/main-core.component';

@NgModule({
  imports: [
    CoreRoutingModule
  ],
  declarations: [
    MainCoreComponent
  ],
  providers: []
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core) {
      throw new Error(`You can't import the CoreModule twice!`);
    }
  }
}
