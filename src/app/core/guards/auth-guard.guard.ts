import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { AuthService } from '@app/core/services/auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(public authService: AuthService, public router: Router) { }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.goToMyPath(childRoute, state);
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.goToMyPath(route, state);
  }

  goToMyPath(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const shouldNavigateToLogin = !this.authService.isUserLogged() || state.url === '/';

    if (shouldNavigateToLogin) {
      this.router.navigate(['login']);
      return false;

    }
    return true;
  
  }

}

@Injectable()
export class LoginGuardService implements CanActivate {

  constructor(public authService: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.goToMyPath();
  }

  goToMyPath(): boolean {
    if (this.authService.isUserLogged()) {
      const mypath = this.authService.getMyInitialPath();
      this.router.navigate([mypath]);
      return false;
    }
    return true;
  }

}
