import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Url } from '@app/core/classes/Url';
import { User } from '../classes/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: User = null;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  login(email: string, password: string): Observable<{ token: string, user: User }> {
    return this.httpClient.post<{ token: string, user: User  }>(Url.LOGIN, { email, password });
  }

  logout() {
    this.removeTokenAndUser();
    this.user = null;
    this.httpClient.post(Url.LOGOUT, {}).subscribe(
      res => setTimeout(() => location.reload(), 1000),
      error => console.log(error)
    );
  }

  verifyToken(): Observable<any> {
    const token = localStorage.getItem('jwt');
    return this.httpClient.post<any>(Url.VERIFY, { token });
  }

  setUser(user: User) {
    this.user = Object.assign(new User, user);
    this.saveUser();

  }

  saveToken(token: any): void {
    localStorage.setItem('jwt', token);
  }

  saveUser(): void {
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  removeTokenAndUser(): void {
    localStorage.removeItem('jwt');
  }

  getUser(): User {
    return this.user;
  }

  isUserLogged(): boolean {
    const userInLocalStorage = Boolean(localStorage.getItem('jwt')) && Boolean(localStorage.getItem('user'));
    if (userInLocalStorage || !this.user) {
      this.getStoredUser();
    }
    return Boolean(this.user);
  }

  getStoredUser(): User {
    if (this.user) {
      return this.user;
    } else if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
      return this.user
    } else {
      return null;
    }
  }

  getMyInitialPath(): string {
    return 'app/menu';
  }

}
