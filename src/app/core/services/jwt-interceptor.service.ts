import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Url } from '../classes/Url';
import { AuthService } from './auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  publicModules = ['participant', 'employee', 'meeting', 'projects'];
  exceptionRoutes = ['/participant/preview'];

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<any>, handler: HttpHandler): Observable<HttpEvent<any>> {
    const jwtToken = localStorage.getItem('jwt');
    const urlModule = this.router.url.split('/')[2];

    const isException = this.exceptionRoutes.some(url => this.router.url.indexOf(url) > -1);
    const isPublicModule = !(this.publicModules.indexOf(urlModule) === -1 || isException);
    const isExternalRoute = !request.url.includes(Url.BASE_URL);

    if (jwtToken && !isPublicModule && !isExternalRoute) {
      const cloned = request.clone({
        'headers': request.headers.set('Authorization', 'JWT ' + jwtToken)
      });
      return this.handlerError(handler.handle(cloned));

    } else if (isPublicModule || isExternalRoute) {
      return handler.handle(request);

    } else {
      return this.handlerError(handler.handle(request));
    }
  }

  handlerError(observable: Observable<HttpEvent<any>>): Observable<any> {
    return observable.pipe(
      catchError(
        error => {

          switch (error.status) {

            case 401:
              this.forceLogout('__authenticateAgainError');
              return throwError(error);

            case 403:
              this.forceLogout('__retrievePermissionError');
              return throwError(error);

            default:
              return throwError(error);
          }
        }
      )
    );
  }

  forceLogout(errorMessage: string) {

    if (this.router.url.indexOf('login') === -1) {

      
      this.authService.logout();

    } else {
      
    }

  }

}
