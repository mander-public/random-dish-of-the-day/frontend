import { MainCoreComponent } from './views/main-core/main-core.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    component: MainCoreComponent,
    children: [
      {
        path: 'app',
        loadChildren: () => import('../layout/layout.module').then(m => m.LayoutModule)
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
