export class User {

  email: string;
  password?: string;
  username: string;
  id: number;
  first_name?: string;
  last_name?: string;
}
