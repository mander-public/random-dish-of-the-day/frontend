import { environment } from '../../../environments/environment';

export class Url {

  static BASE_URL = environment.BASE_URL;
  static BASE_API: string = Url.BASE_URL.concat('api/v1/');
  static WS_URL = environment.WS_URL.concat('ws/');

  /* AUTH */

  static LOGIN: string = Url.BASE_API + 'login/';
  static CONFIRM_LOGIN_PIN: string = Url.LOGIN + 'confirm/';
  static LOGOUT: string = Url.BASE_API + 'logout/';
  static VERIFY: string = Url.BASE_API + 'jwt-verify/';

  static RESET_PASSWORD: string = Url.BASE_API + 'password/reset/';
  static CONFIRM_NEW_PASSWORD: string = Url.BASE_API + 'password/reset/confirm/';

  /* TOPICS */

  static TOPICS: string = Url.BASE_API + 'topic/';
  static TOPICS_DETAIL: string = Url.BASE_API + 'topic/{id}/';

  /* INGREDIENTS */

  static INGREDIENTS: string = Url.BASE_API + 'ingredient/';
  static INGREDIENTS_DETAIL: string = Url.INGREDIENTS + '{uuid}/';

  /* DISHES */

  static DISHES: string = Url.BASE_API + 'dish/';
  static DISHES_DETAIL: string = Url.DISHES + '{uuid}/';
  static IMPORT_DISH: string = Url.DISHES + 'import/';


}
