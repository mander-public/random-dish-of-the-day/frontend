import { Filter } from "./filter";

export class Utils {
    constructor() { }

    static buildFilter(...filters: Filter[]): string {
        const serializeFilters = this._deestructureArrayFilters(filters);
        const query = serializeFilters.length > 0 ? `?${serializeFilters.filter(filter => filter.value)
        .map(filter => `${filter.name}=${filter.value}`).join('&')}` : '';
        return query;
    }

    static setFilter(parameters: Filter[], filter: Filter): Filter[] {
        const element = parameters.find(parameter => parameter.name === filter.name);
        const newParameters = parameters.slice();
        if (element) {
        element.value = filter.value;
        } else {
        newParameters.push(filter);
        }
        return newParameters;
    }

    static _deestructureArrayFilters(filters: any): { name: string, value: string | string[] }[] {
        const valuesString = filters.filter((filter: { value: any; }) => !Array.isArray(filter.value));
        const valuesArray = filters.filter((filter: { value: any; }) => Array.isArray(filter.value));
        const totalFilter = valuesString;
        valuesArray.forEach((filter: { value: any[]; name: any; }) => totalFilter.push(...filter.value.map((value: any) => ({ name: filter.name, value }))));
        return totalFilter;
    }
}