export class Filter {
    name: string;
    value: number | string | string[];
  
    constructor(name: string, value: number | string | string[]) {
      this.name = name;
      this.value = value;
    }
  
    public getFilterValues(): Filter[] {
      const values = Array.isArray(this.value) ? this.value : [this.value];
      return values.map(value => new Filter(this.name, value));
    }
  }

  export function buildFilter(...filters: Filter[]): string {
    const serializeFilters = _deestructureArrayFilters(filters);
    const query = serializeFilters.length > 0 ? `?${serializeFilters.filter(filter => filter.value)
      .map(filter => `${filter.name}=${filter.value}`).join('&')}` : '';
    return query;
  }
  
  export function setFilter(parameters: Filter[], filter: Filter): Filter[] {
    const element = parameters.find(parameter => parameter.name === filter.name);
    const newParameters = parameters.slice();
    if (element) {
      element.value = filter.value;
    } else {
      newParameters.push(filter);
    }
    return newParameters;
  }

  export function _deestructureArrayFilters(filters: Filter[]): Filter[] {
    let totalFilter: any[] = [];
    filters.forEach(filter => { totalFilter = totalFilter.concat(filter.getFilterValues()) });
    return totalFilter;
  }