import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '@app/core/guards/auth-guard.guard';
import { CreateDishComponent } from './views/create-dish/create-dish.component';
import { DetailDishComponent } from './views/detail-dish/detail-dish.component';
import { ListDishComponent } from './views/list-dish/list-dish.component';


const routes: Routes = [
  {
    component: ListDishComponent,
    path: '',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    children: []
  },
  {
    component: CreateDishComponent,
    path: 'create-dish',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    children: []
  },
  {
    path: ":uuid",
    component: DetailDishComponent,
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    children: []
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishRoutingModule { }
