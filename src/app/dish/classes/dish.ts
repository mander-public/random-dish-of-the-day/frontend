import { User } from "@app/core/classes/User";
import { Ingredient } from "@app/ingredient/classes/ingredient";

export class Dish {
    id:number;
    uuid: string;
    name: string = '';
    description: string = '';
    editing: boolean = false;
    owner: User;
    calories: number;
    recipe: string;
    ingredients: Ingredient[];
}
