import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Dish } from '@app/dish/classes/dish';
import { DishService } from '@app/dish/services/dish.service';
import { Ingredient } from '@app/ingredient/classes/ingredient';
import { IngredientService } from '@app/ingredient/services/ingredient.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-create-dish',
  templateUrl: './create-dish.component.html',
  styleUrls: ['./create-dish.component.scss']
})
export class CreateDishComponent implements OnInit {
  ingredients: Ingredient[];
  dish:Dish = new Dish();
  options:string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  mainIngredientControl = new FormControl('');

  constructor(
    private ingredientService: IngredientService,
    private dishService: DishService
    ) { }

  ngOnInit(): void {
    this.getListOfIngredients();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  getListOfIngredients() {
    this.ingredientService.lightListIngredients().subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
        this.options = this.ingredients.map(ingredient => ingredient.name);
        this.filteredOptions = this.mainIngredientControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value || '')),
        );
      },
      error => console.log(error)
    );
  }

  createDish() {
    let selectedIngredient = this.mainIngredientControl.value;
    let selectedIngredients = this.ingredients.filter(ingredient => ingredient.name == selectedIngredient);
    this.dish.ingredients = selectedIngredients;
    this.dishService.createDish(this.dish).subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      },
      error => console.log(error)
    );
  }

}
