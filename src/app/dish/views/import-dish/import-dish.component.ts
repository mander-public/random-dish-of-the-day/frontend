import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../list-dish/list-dish.component';

@Component({
  selector: 'app-import-dish',
  templateUrl: './import-dish.component.html',
  styleUrls: ['./import-dish.component.scss']
})
export class ImportDishComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ImportDishComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}