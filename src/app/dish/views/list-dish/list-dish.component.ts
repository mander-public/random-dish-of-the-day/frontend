import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Filter, setFilter } from '@app/core/classes/filter';
import { Dish } from '@app/dish/classes/dish';
import { DishService } from '@app/dish/services/dish.service';
import { ImportDishComponent } from '../import-dish/import-dish.component';

export interface DialogData {
  code: string;
}


@Component({
  selector: 'app-list-dish',
  templateUrl: './list-dish.component.html',
  styleUrls: ['./list-dish.component.scss']
})
export class ListDishComponent implements OnInit {
  dishes: {
    lastLoadedPage: number,
    lastPage: number,
    loading: boolean,
    total: number,
    dishes: Dish[]
  } = {
    lastLoadedPage: 1,
    lastPage: 1,
    loading: true,
    total: 0,
    dishes: [],
  };
  searchValue: string = null;
  dishesFilter: Filter[] = [];
  code:string;

  constructor(
    private dishService: DishService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.dishesFilter = setFilter(this.dishesFilter, new Filter('page', 1))
    this.getDishes();
  }

  getDishes () {
    this.dishService.listDishes(...this.dishesFilter).subscribe(
      (dishes) => {
        this.dishes.lastPage = parseInt(dishes.headers.get('last-page'), 0);
        this.dishes.total = parseInt(dishes.headers.get('total'), 0);
        this.dishes.dishes = dishes.body;
        this.dishes.dishes.map(ingredient => ingredient.editing = false);
        this.dishes.loading = false;
      },
      error => console.log(error)
    );
  }

  importDish(): void {
    const dialogRef = this.dialog.open(ImportDishComponent, {
      width: '600px',
      data: {code: this.code},
    });

    dialogRef.afterClosed().subscribe((result:string) => {
      this.code = result;
      this.dishService.importDish(this.code).subscribe(
        (response) => {
          console.log(response);
          this.getDishes();
        },
        error => console.log(error)
      );
    });
  }

  onSearchChange(key: KeyboardEvent, value: string){
    if (key.code === 'Enter') {
      this.searchValue = value;
      const filter = new Filter('search', this.searchValue);
      this.dishesFilter = setFilter(this.dishesFilter, filter);
      this.dishes.lastLoadedPage = 1;
      this.getDishes();
      }
    if (value === "") {
      this.dishesFilter = this.dishesFilter.filter(filter=> filter.name != "search");
      this.getDishes();
      this.dishes.lastLoadedPage = 1;
    }
  }

  loadMoreDishes(page: any) {
    this.dishes.lastLoadedPage = page.pageIndex + 1;
    this.setPageFilter(page.pageIndex + 1);
    this.getDishes();
  }

  setPageFilter(page: number) {
    this.dishes.lastLoadedPage = page;
    this.dishesFilter = setFilter(this.dishesFilter, new Filter('page', page));
  }

}