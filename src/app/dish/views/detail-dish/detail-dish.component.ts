import { Clipboard } from '@angular/cdk/clipboard';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dish } from '@app/dish/classes/dish';
import { DishService } from '@app/dish/services/dish.service';

@Component({
  selector: 'app-detail-dish',
  templateUrl: './detail-dish.component.html',
  styleUrls: ['./detail-dish.component.scss']
})
export class DetailDishComponent implements OnInit {

  dishUUID: string = "";
  dish: Dish;

  constructor(
    private dishService: DishService,
    private activatedRoute: ActivatedRoute,
    private clipboard: Clipboard
  ) {
    this.dishUUID = this.activatedRoute.snapshot.paramMap.get('uuid');
  }

  ngOnInit(): void {
    this.getDish();
  }

  getDish() {
    this.dishService.getDish(this.dishUUID).subscribe(
      (dish: Dish) => {
        this.dish = dish;
      },
      error => console.log(error)
    );
  }

  shareDish() {
    this.clipboard.copy(this.dishUUID);
  }
}
