import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDishComponent } from './views/list-dish/list-dish.component';
import { DetailDishComponent } from './views/detail-dish/detail-dish.component';
import { MatIconModule } from '@angular/material/icon';
import { ColorPickerModule } from 'ngx-color-picker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DishRoutingModule } from './dish-routing.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { CreateDishComponent } from './views/create-dish/create-dish.component'; 
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field'; 
import { MatInputModule } from '@angular/material/input';
import { ImportDishComponent } from './views/import-dish/import-dish.component';
import { MatSelectModule } from '@angular/material/select'; 
import { MatChipsModule } from '@angular/material/chips'; 
import { MatMenuModule } from '@angular/material/menu';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [ListDishComponent, DetailDishComponent, CreateDishComponent, ImportDishComponent],
  imports: [
    CommonModule,
    DishRoutingModule,
    MatIconModule,
    ColorPickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatCardModule,
    ClipboardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatMenuModule,
    MatAutocompleteModule,
    MatPaginatorModule,
  ],
  exports: [
    MatFormFieldModule
  ]
})
export class DishModule { }
