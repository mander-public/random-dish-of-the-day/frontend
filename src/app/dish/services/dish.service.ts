import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Url } from '@app/core/classes/Url';
import { Dish } from '../classes/dish';
import { Filter } from '@app/core/classes/filter';
import { Utils } from '@app/core/classes/utils';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(
    private httpClient: HttpClient
  ) { }

  listDishes(...filters: Filter[]) {
    const query = Utils.buildFilter(...filters);
    const url = `${Url.DISHES}`;
    return this.httpClient.get<any>(url + query, { observe: 'response' });
  }

  getDish(uuid: string) {
    const url = `${Url.DISHES_DETAIL.replace(`{uuid}`, uuid)}`;
    return this.httpClient.get<Dish>(url);
  }

  createDish(dish: Dish) {
    const url = `${Url.DISHES}`;
    return this.httpClient.post<any>(url, dish);
  }

  importDish(code: string) {
    const url = `${Url.IMPORT_DISH}`;
    return this.httpClient.post<any>(url, {"uuid": code});  
  }
}
