import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Url } from '@app/core/classes/Url';


@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  constructor(private httpClient: HttpClient) { }

  resetPassword(email: string): Observable<any> {
    return this.httpClient.post(Url.RESET_PASSWORD, {email});
  }

  newPassword(token: any, uid: any, new_password1: any, new_password2: any): Observable<any> {
    return this.httpClient.post(Url.CONFIRM_NEW_PASSWORD, { token, uid, new_password1, new_password2 });
  }
}
