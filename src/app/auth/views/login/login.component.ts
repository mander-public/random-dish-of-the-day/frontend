import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/core/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email = '';
  password = '';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.authService.login(this.email, this.password).subscribe(
      response => {
        this.authService.setUser(response.user);
        this.authService.saveToken(response.token);
        this.navigateToMyPath();
      },
      error => console.log(error)
    );
  }

  navigateToMyPath(): void {
    this.router.navigate([this.authService.getMyInitialPath()]);
  }

}
