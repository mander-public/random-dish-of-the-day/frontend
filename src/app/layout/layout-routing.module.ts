import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '@app/core/guards/auth-guard.guard';
import { LayoutComponent } from './views/layout/layout.component';

const routes: Routes = [
  {
    component: LayoutComponent,
    path: '',
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    children: [
      {
        path: 'ingredients',
        loadChildren: () => import('app/ingredient/ingredient.module').then(m => m.IngredientModule)
      },
      {
        path: 'dishes',
        loadChildren: () => import('app/dish/dish.module').then(m => m.DishModule)
      },
      {
        path: 'menu',
        loadChildren: () => import('app/menu/menu.module').then(m => m.MenuModule)
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
