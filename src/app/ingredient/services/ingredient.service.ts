import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Filter } from '@app/core/classes/filter';
import { Url } from '@app/core/classes/Url';
import { Utils } from '@app/core/classes/utils';
import { Ingredient } from '../classes/ingredient';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  constructor(
    private httpClient: HttpClient,
  ) { }

  listIngredients(...filters: Filter[]) {
    const query = Utils.buildFilter(...filters);
    const url = `${Url.INGREDIENTS}`;
    return this.httpClient.get<any>(url + query, { observe: 'response' });
  }
  
  lightListIngredients() {
    const url = `${Url.INGREDIENTS}`;
    return this.httpClient.get<Ingredient[]>(url);
  }
  
  deleteIngredient(uuid: string) {
    const url = `${Url.INGREDIENTS_DETAIL.replace(`{uuid}`, uuid)}`;
    return this.httpClient.delete<any>(url);
  }

  updateIngredient(ingredient: Ingredient) {
    const url = `${Url.INGREDIENTS_DETAIL.replace(`{uuid}`, ingredient.uuid)}`;
    return this.httpClient.put<Ingredient>(url, ingredient);
  }

  createIngredient(ingredient: Ingredient) {
    const url = `${Url.INGREDIENTS}`;
    return this.httpClient.post<Ingredient>(url, ingredient);
  }

}
