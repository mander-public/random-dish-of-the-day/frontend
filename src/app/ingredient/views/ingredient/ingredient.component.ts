import { Component, OnInit } from '@angular/core';
import { Filter, setFilter } from '@app/core/classes/filter';
import { Ingredient } from '@app/ingredient/classes/ingredient';
import { IngredientService } from '@app/ingredient/services/ingredient.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {
  ingredients: {
    lastLoadedPage: number,
    lastPage: number,
    loading: boolean,
    total: number,
    ingredients: Ingredient[]
  } = {
    lastLoadedPage: 1,
    lastPage: 1,
    loading: true,
    total: 0,
    ingredients: [],
  };
  
  searchValue: string = null;
  ingredientsFilter: Filter[] = [];

  readonly: boolean = false;
  creatingIngredient: boolean = false;
  createIngredient: Ingredient = new Ingredient();
  createIngredientName = '';


  constructor(
    private ingredientService: IngredientService
  ) { }

  ngOnInit(): void {
    this.ingredientsFilter = setFilter(this.ingredientsFilter, new Filter('page', 1))
    this.getListOfIngredients();
  }

  getListOfIngredients() {
    this.ingredientService.listIngredients(...this.ingredientsFilter).subscribe(
      (ingredients) => {
        this.ingredients.lastPage = parseInt(ingredients.headers.get('last-page'), 0);
        this.ingredients.total = parseInt(ingredients.headers.get('total'), 0);
        this.ingredients.ingredients = ingredients.body;
        this.ingredients.ingredients.map(ingredient => ingredient.editing = false);
        this.ingredients.loading = false;
      },
      error => console.log(error)
    );
  }

  editIngredient(Ingredient: Ingredient) {
    Ingredient.editing = true;
    this.readonly = false;
  }

  cancelEditIngredient(Ingredient: Ingredient) {
    Ingredient.editing = false;
    this.readonly = true;
  }

  cancelCreateIngredient() {
    this.creatingIngredient = false;
  }

  deleteIngredient(ingredient: Ingredient) {
    this.ingredientService.deleteIngredient(ingredient.uuid).subscribe(
      (response) => {
        this.getListOfIngredients();
      },
      error => console.log(error)
    );
  }

  saveIngredient(ingredient: Ingredient) {
    this.ingredientService.updateIngredient(ingredient).subscribe(
      (response) => {
        this.getListOfIngredients();
      },
      error => console.log(error)
    );
  }
  
  postIngredient(Ingredient: Ingredient, name:string) {
    Ingredient.name = name;
    this.ingredientService.createIngredient(Ingredient).subscribe(
      (response) => {
        this.creatingIngredient = false;
        this.createIngredient.editing = false;
        this.getListOfIngredients();
      },
      error => console.log(error)
    );
  }

  showCreateIngredient() {
    this.creatingIngredient = true;
    this.createIngredient.editing = true;
  }

  onSearchChange(key: KeyboardEvent, value: string){
    if (key.code === 'Enter') {
      this.searchValue = value;
      const filter = new Filter('search', this.searchValue);
      this.ingredientsFilter = setFilter(this.ingredientsFilter, filter);
      this.ingredients.lastLoadedPage = 1;
      this.getListOfIngredients();
      }
    if (value === "") {
      this.ingredientsFilter = this.ingredientsFilter.filter(filter=> filter.name != "search");
      this.getListOfIngredients();
      this.ingredients.lastLoadedPage = 1;
    }
  }

  loadMoreIngredients(page: any) {
    this.ingredients.lastLoadedPage = page.pageIndex + 1;
    this.setPageFilter(page.pageIndex + 1);
    this.getListOfIngredients();
  }

  setPageFilter(page: number) {
    this.ingredients.lastLoadedPage = page;
    this.ingredientsFilter = setFilter(this.ingredientsFilter, new Filter('page', page));
  }
}

