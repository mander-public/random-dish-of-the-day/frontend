import { User } from "@app/core/classes/User";

export class Ingredient {
    id:number;
    color: string = '#000000';
    uuid: string;
    name: string = '';
    editing: boolean = false;
    owner: User;
}